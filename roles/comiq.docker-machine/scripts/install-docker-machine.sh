#!/bin/bash

VERSION=$1
INSTALLFILE=/usr/local/bin/docker-machine
URL=https://github.com/docker/machine/releases/download/v$VERSION/docker-machine-`uname -s`-`uname -m`

if [ -f $INSTALLFILE ]; then
  CURRENT_VERSION=$($INSTALLFILE --version)
else
  CURRENT_VERSION="Not installed"
fi
echo $CURRENT_VERSION | grep " $VERSION,"
SAMEVERSION=$?

if [ $SAMEVERSION == 1 ]; then
  OUT="$(mktemp)"

  # Handle interruption
  trap ctrl_c INT
  function ctrl_c() {
    echo "\nDownload interrupted"
    if [ -f "$OUT" ]; then
      rm $OUT
      echo "Removed temporary file '$OUT'"
    fi
    exit -1
  }

  # Download to temporary location
  echo "Download from '$URL' to temporary file '$OUT'"
  curl -L $URL > $OUT
  if [ $? != 0 ]; then
    rm $OUT
    echo "Download failed from '$URL'. Removed temorary file '$OUT'"
    exit -1
  fi

  # Move downloaded file to binary folder
  rm -f $INSTALLFILE
  mv $OUT $INSTALLFILE
  chmod ugo+rx $INSTALLFILE

  exit 1
else
  exit 0
fi
